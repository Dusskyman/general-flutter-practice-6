import 'package:flutter/material.dart';
import 'package:general_flutter_practice_6/models/simple_model.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TimePick extends StatefulWidget {
  @override
  _TimePickState createState() => _TimePickState();
}

class _TimePickState extends State<TimePick> {
  TimeOfDay _timeOfDay = TimeOfDay.now();

  @override
  Widget build(BuildContext context) {
    var val = Provider.of<Data>(context, listen: false);
    return Container(
      decoration: BoxDecoration(
          border: Border.all(width: 2),
          borderRadius: BorderRadius.circular(10)),
      width: 80,
      height: 40,
      child: TextButton(
        child: val.time == null
            ? Text('${_timeOfDay.hour}:${_timeOfDay.minute}')
            : Text('${val.time.hour}:${val.time.minute}'),
        onPressed: () {
          showTimePicker(context: context, initialTime: val.time ?? _timeOfDay)
              .then(
            (value) => setState(() {
              val.time = value;
            }),
          );
        },
      ),
    );
  }
}
