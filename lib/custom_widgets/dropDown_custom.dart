import 'package:flutter/material.dart';
import 'package:general_flutter_practice_6/models/simple_model.dart';
import 'package:provider/provider.dart';

class CustomDropDown extends StatefulWidget {
  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  @override
  Widget build(BuildContext context) {
    var val = Provider.of<Data>(context, listen: false);
    return Container(
      width: 100,
      height: 40,
      child: DropdownButton(
        value: val.gender,
        items: ['Male', 'Female']
            .map(
              (e) => DropdownMenuItem(
                value: e,
                child: Text(e),
              ),
            )
            .toList(),
        onChanged: (value) {
          setState(() {
            val.gender = value;
          });
        },
      ),
    );
  }
}
