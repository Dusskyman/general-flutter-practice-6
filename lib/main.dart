import 'package:flutter/material.dart';
import 'package:general_flutter_practice_6/custom_widgets/datepicker.dart';
import 'package:general_flutter_practice_6/custom_widgets/dropDown_custom.dart';
import 'package:general_flutter_practice_6/custom_widgets/slider_custom.dart';
import 'package:general_flutter_practice_6/custom_widgets/timepicker.dart';
import 'package:general_flutter_practice_6/models/simple_model.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: Data(),
      builder: (context, child) => MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var val = Provider.of<Data>(context, listen: false);
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Выберите время: ",
                    style: TextStyle(fontSize: 20),
                  ),
                  TimePick(),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Выберите дату: ",
                    style: TextStyle(fontSize: 20),
                  ),
                  DatePick(),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Выберите значение: ",
                    style: TextStyle(fontSize: 20),
                  ),
                  CustomSlider(),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Выберите ваш пол: ",
                    style: TextStyle(fontSize: 20),
                  ),
                  CustomDropDown(),
                ],
              ),
              IconButton(
                  icon: Icon(Icons.save),
                  onPressed: () {
                    SimpleModel simpleModel = SimpleModel(
                        value: val.value,
                        time: val.time.toString(),
                        data: val.date.toString(),
                        gender: val.gender);
                    simpleModel.readAll();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
