import 'package:flutter/material.dart';
import 'package:general_flutter_practice_6/models/simple_model.dart';
import 'package:provider/provider.dart';

class CustomSlider extends StatefulWidget {
  @override
  _CustomSliderState createState() => _CustomSliderState();
}

class _CustomSliderState extends State<CustomSlider> {
  @override
  Widget build(BuildContext context) {
    var val = Provider.of<Data>(context, listen: false);
    return Container(
      width: 80,
      height: 40,
      child: Slider.adaptive(
        min: 0,
        max: 1,
        value: val.value.floorToDouble(),
        onChanged: (v) {
          setState(() {
            val.value = v.toInt();
          });
        },
      ),
    );
  }
}
