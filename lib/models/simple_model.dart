import 'package:flutter/material.dart';

class Data with ChangeNotifier {
  TimeOfDay time;
  DateTime date;
  int value = 0;
  String gender = 'Male';
}

class SimpleModel {
  final String data;
  final String time;
  final int value;
  final String gender;

  SimpleModel({this.data, this.time, this.value, this.gender});

  void readAll() {
    print('Data: $data, Time: $time, Value: $value, Gender: $gender.');
  }
}
