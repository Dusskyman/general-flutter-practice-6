import 'package:flutter/material.dart';
import 'package:general_flutter_practice_6/models/simple_model.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DatePick extends StatefulWidget {
  @override
  _DatePickState createState() => _DatePickState();
}

class _DatePickState extends State<DatePick> {
  DateTime _initialDate = DateTime.now();

  var _format = DateFormat.yMd();

  @override
  Widget build(BuildContext context) {
    var val = Provider.of<Data>(context, listen: false);
    return Container(
      decoration: BoxDecoration(
          border: Border.all(width: 2),
          borderRadius: BorderRadius.circular(10)),
      width: 100,
      height: 40,
      child: TextButton(
        child: Text(
          _format.format(val.date ?? _initialDate),
        ),
        onPressed: () {
          showDatePicker(
            context: context,
            initialDate: val.date ?? _initialDate,
            firstDate: DateTime(2000),
            lastDate: DateTime(2030),
          ).then((value) => setState(() {
                val.date = value;
              }));
        },
      ),
    );
  }
}
